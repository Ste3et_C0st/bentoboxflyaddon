package de.Ste3et_C0st.BentoBoxFly;

import java.util.Objects;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.ComponentBuilder;
import world.bentobox.bentobox.BentoBox;
import world.bentobox.bentobox.api.addons.Addon;
import world.bentobox.bentobox.api.addons.AddonDescription;
import world.bentobox.bentobox.api.flags.Flag;
import world.bentobox.bentobox.api.flags.Flag.Mode;
import world.bentobox.bentobox.api.user.User;
import world.bentobox.bentobox.database.objects.Island;

public class bentoBoxFlyMain extends Addon{
	
	private static bentoBoxFlyMain instance;
	private Flag flyFlag;
	private boolean hooked =false;
	private String permission,feedBack,stateON,stateOFF,feedbackDispaly;
	
	public void onEnable() {
		instance = this;
		this.setDescription(new AddonDescription.Builder("BentoBoxFly", "Allow players to fly on their island", "0.1").build());
		this.flyFlag = new Flag.Builder("FLY", Material.FEATHER).mode(Mode.BASIC).addon(getInstance()).build();
		this.hooked = true;
		this.registerFlag(flyFlag);
		this.registerListener(new FlyListeners());
		saveDefaultConfig();
		permission = getConfig().getString("flyConfig.permissions");
		feedBack = getConfig().getString("flyConfig.feedback");
		stateON = getConfig().getString("flyConfig.stateOn");
		stateOFF = getConfig().getString("flyConfig.stateOff");
		feedbackDispaly = getConfig().getString("flyConfig.feedbackDispaly", "CHAT").toUpperCase();
	}
	
	public String getFeedbackString() {
		return this.feedBack;
	}
	
	public void sendFeedBack(Player player, boolean bool) {
		if(!feedbackDispaly.equalsIgnoreCase("NONE")) {
			String feedback = bentoBoxFlyMain.getInstance().getFeedbackString().replace("%STATE%", bool ? stateON : stateOFF);
			feedback = ChatColor.translateAlternateColorCodes('&', feedback);
			player.spigot().sendMessage(feedbackDispaly.equals("ACTIONBAR") ? ChatMessageType.ACTION_BAR : ChatMessageType.CHAT, new ComponentBuilder(feedback).create());
		}
	}
	
	public static bentoBoxFlyMain getInstance() {
		return instance;
	}
	
	public Island getIsland(Location loc){
		return BentoBox.getInstance().getIslands().getProtectedIslandAt(loc).orElse(null);
	}
	
	public boolean isIsland(Location loc) {
		return getIsland(loc) != null;
	}
	
	public boolean isIslandWorld(World w) {
		return BentoBox.getInstance().getIWM().inWorld(w);
	}
	
	public boolean isIslandWorld(Location loc) {
		return isIslandWorld(loc.getWorld());
	}
	
	public boolean canFlyOnLocation(Location loc, UUID uuid){
		if(!isIslandWorld(loc)) return false;
		Island island = getIsland(loc);
		return Objects.nonNull(island) && (island.isAllowed(User.getInstance(uuid), this.flyFlag));
	}
	
	public boolean hasFly(Player p) {
		return p.getAllowFlight();
	}
	
	public boolean canFly(Location loc, Player player) {
		return canFlyOnLocation(loc, player.getUniqueId()) && player.hasPermission(this.permission);
	}
	
	public boolean canFly(Location loc, User user) {
		return canFly(loc, user.getPlayer());
	}

	@Override
	public void onDisable() {
		// TODO Auto-generated method stub
		
	}
	
}