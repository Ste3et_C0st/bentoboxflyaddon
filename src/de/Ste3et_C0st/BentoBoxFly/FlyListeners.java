package de.Ste3et_C0st.BentoBoxFly;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.minecraft.server.v1_14_R1.ChatClickable;
import world.bentobox.bentobox.api.events.island.IslandEvent;
import world.bentobox.bentobox.api.user.User;

public class FlyListeners implements Listener {
	
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	public void onIslandTeleport(PlayerTeleportEvent e) {
		User user = User.getInstance(e.getPlayer());
		boolean to = bentoBoxFlyMain.getInstance().canFly(e.getTo(), user);
		if(to && !e.getPlayer().getAllowFlight()) {
			if(!bentoBoxFlyMain.getInstance().hasFly(e.getPlayer())) {
				sendMessage(e.getPlayer(), true);
			}
		}else if(!to && e.getPlayer().getAllowFlight()) {
			sendMessage(e.getPlayer(), false);
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	public void onIslandExitEvent(IslandEvent.IslandExitEvent e) {
		User user = User.getInstance(e.getOwner());
		if(user.getPlayer().getAllowFlight()) {
			Bukkit.getScheduler().runTaskLaterAsynchronously(bentoBoxFlyMain.getInstance().getPlugin(), () -> {
				Player player = user.getPlayer();
				if(bentoBoxFlyMain.getInstance().hasFly(player)) {
					if(!bentoBoxFlyMain.getInstance().canFly(player.getLocation(), player)) {
						sendMessage(player, false);
					}
				}
			}, 20*5);
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	public void onIslandEnterEvent(IslandEvent.IslandEnterEvent e) {
		User user = User.getInstance(e.getOwner());
		if(!user.getPlayer().getAllowFlight()) {
			Player player = user.getPlayer();
			Bukkit.getScheduler().runTaskLaterAsynchronously(bentoBoxFlyMain.getInstance().getPlugin(), () -> {
				if(!bentoBoxFlyMain.getInstance().hasFly(player)) {
					if(bentoBoxFlyMain.getInstance().canFly(player.getLocation(), player)) {
						sendMessage(player, true);
					}
				}
			}, 2L);
		}
	}
	
	public void sendMessage(User user, boolean b) {
		sendMessage(user.getPlayer(), b);
	}
	
	public void sendMessage(Player player, boolean b) {
		bentoBoxFlyMain.getInstance().sendFeedBack(player, b);
		player.setAllowFlight(b);
	}
}
